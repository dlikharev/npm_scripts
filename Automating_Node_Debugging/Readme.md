**Node --inspect**

*start:debug*
`nodemon --inspect index.js`
`nodemon --inspect-brk index.js` - allow to make breakpoints on importing/requiring modules

`npm run start:debug`

Debugger is listening to the process

*Command pal*
CTRL + Shift + P 

Attach to node process and find the right pwd to the directory of the application