**NPM script phases**

`"prestart": "npm run <command> && ..."`

**Pass attributes**
Cross-env - run scripst and set env vars iacross platforms

package.json config keys are overwritten in the environment if there is a config param of
`{ "name": "foo" , "config": { "port": "8000" } , "scripts": { "start": "node server.js" } }`

server.js is

`http.createServer(...).listen(process.env.npm_package_config_port)`

OR

`npm config set foo:port 80`
