(function () {
  const name = document.getElementById('name');
  name.innerText = 'Pluralsight';

  import(/* webpackChunkName: "lodash" */ 'lodash').then( ({default: _}) => {
    const message = document.getElementById('message');
    message.innerText = _.join(['Hello', ' Lirik']);
  });
})()