const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const outputDirectory = path.resolve(__dirname, 'dist');
const entry = path.join(__dirname, './src/client/', 'client.js');

module.exports = {
  mode: 'development',
  entry: entry,
  output: {
    filename: 'bundle.js',
    chunkFilename: '[name].vendor.js'
  },
  plugins: [
    new CleanWebpackPlugin(),
    new CopyPlugin({
      patterns: [
        { from: './src/client/**/*', to: outputDirectory, flatten: true, globOptions: { ignore: ['*.js'] } },
        { from: './src/server/app.js', to: outputDirectory + '/app.js', flatten: true },
      ]
    })
  ],
};