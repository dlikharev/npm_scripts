import express from "express";

const app = express();
const port = process.env.npm_package_config_port || 4005;
let runningMessage = "Server is running on port " + port;

app.get("/", (req: any, res: { send: (arg0: string) => void }) => {
  console.log("API was successfully requested");
  res.send(runningMessage);
});

const server = app.listen(port, () => {
  console.log(runningMessage);
});

module.exports = server;
