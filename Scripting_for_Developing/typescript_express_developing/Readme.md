**TIPS**

For *start:dev* we need *prestart:dev* script to be present in *package.json* file

`npm run-script <command> [--silent] [-- <args>...]`
`alias: npm run`

As of npm@2.0.0, you can use custom arguments when executing scripts. The special option -- is used by getopt to delimit the end of the options. npm will pass all the arguments after the -- directly to your script:

`npm run test -- --grep="pattern"`

**Compile TYPESCRIPT and Node app in parallel**

npm-run-all - cli tool to run NPM multiple scripts in parralel or in sequence

**Another way to develop in Typescript**

ts-node-dev
