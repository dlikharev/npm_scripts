#!/bin/bash

# stop all containers
docker stop --force $(docker ps -a -q)

# destroy all containers
docker rm --force $(docker ps -a -q)

# destroy all images
docker rmi --force $(docker images -q)


## Give execute permission to your script:

## chmod +x /path/to/yourscript.sh

## And to run your script:

## /path/to/yourscript.sh