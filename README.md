**NPM scrips understanding**

npm run script

In Addition to the SHELL's pre existing PATH, `npm run` adds node_modules/.bin to the PATH provided to scripts. 
Gives access to 3-rd party libs, we may want to use.

**Synopsis**
`npm run-script <command> [--silent] [-- <args>...]`